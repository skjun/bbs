<?php if (!defined('APPLICATION')) exit();

//This plugin is based on digibomb ShareThis plugin for vanilla. I added comments support and 2.1b1 support.

// Define the plugin:
$PluginInfo['VShare'] = array(
   'Name' => 'VShare',
   'Description' => 'This plugin adds ShareThis (http://sharethis.com) buttons to the bottom of each post. Based on digibomb ShareThis Plugin. Works on all versions of Vanilla.Puts the buttons on the comments and discussions.',
   'Version' => '1.1',
   'SettingsUrl' => '/dashboard/plugin/VShare',
   'SettingsPermission' => 'Garden.Settings.Manage',
   'RegisterPermissions' => FALSE,
   'MobileFriendly'=> TRUE,
   'Author' => "VrijVlinder",
   'AuthorEmail' => 'contact@vrijvlinder.com',
   'AuthorUrl' => 'http://www.vrijvlinder.com'
);


class VSharePlugin extends Gdn_Plugin {

   public function DiscussionController_AfterCommentBody_Handler($Sender) {
   
      /* Add CSS  for share div */
      
      $Sender->AddCSSFile('plugins/VShare/design/vshare.css');
   
      /* Get the VShare publisher number. */
      $PublisherNumber = C('Plugin.VShare.PublisherNumber', 'Publisher Number');
      
      $VShareCode = '<div class="VShare"><span class="st_fblike_large" displayText="Like"></span><span class="st_sharethis_large" displayText="ShareThis"</span><span class="st_wordpress_large" displayText="WordPress"></span><span class="st_facebook_large" displayText="F"</span><span class="st_pinterest_large" displayText="Pinterest"></span><span class="st_reddit_large" displayText="Reddit"></span><span class="st_email_large" displayText="Email"></span></div>';  
   
      /* Add javascript for VShare */

      $Sender->Head->AddString("<script type=\"text/javascript\">var switchTo5x=true;</script><script type=\"text/javascript\" src=\"http://w.sharethis.com/button/buttons.js\"></script><script type=\"text/javascript\">stLight.options({publisher:'{$PublisherNumber},doNotHash: false, doNotCopy: false, hashAddressBar: false'});</script>");
      /* Display VShare buttons. */
      
      if (GetValue('Type', $Sender->EventArguments) == 'Comment') {
         echo $VShareCode;
      }
   }     
public function DiscussionController_AfterComment_Handler($Sender) {
     /* Add CSS  for share div */
      
      $Sender->AddCSSFile('plugins/VShare/design/vshare.css');
   
      /* Get the VShare publisher number. */
      $PublisherNumber = C('Plugin.VShare.PublisherNumber', 'Publisher Number');
      
      $VShareCode = '<div class="VShare"><span class="st_fblike_large" displayText="Like"></span><span class="st_sharethis_large" displayText="ShareThis"</span><span class="st_wordpress_large" displayText="WordPress"></span><span class="st_facebook_large" displayText="F"</span><span class="st_pinterest_large" displayText="Pinterest"></span><span class="st_reddit_large" displayText="Reddit"></span><span class="st_email_large" displayText="Email"></span></div>';  
   
      /* Add javascript for VShare */

      $Sender->Head->AddString("<script type=\"text/javascript\">var switchTo5x=true;</script><script type=\"text/javascript\" src=\"http://w.sharethis.com/button/buttons.js\"></script><script type=\"text/javascript\">stLight.options({publisher:'{$PublisherNumber},doNotHash: false, doNotCopy: false, hashAddressBar: false'});</script>");
      /* Display VShare buttons. */
      
      if (GetValue('Type', $Sender->EventArguments) != 'Comment') {
         echo $VShareCode;
      }
   }     



public function DiscussionController_AfterDiscussionBody_Handler($Sender) {
   
      /* Add CSS  for share div */
      
      $Sender->AddCSSFile('plugins/VShare/design/vshare.css');
   
      /* Get the VShare publisher number. */
      $PublisherNumber = C('Plugin.VShare.PublisherNumber', 'Publisher Number');
      
      $VShareCode = '<div class="VShare"><span class="st_fblike_large" displayText="Like"></span><span class="st_sharethis_large" displayText="ShareThis"</span><span class="st_wordpress_large" displayText="WordPress"></span><span class="st_facebook_large" displayText="F"</span><span class="st_pinterest_large" displayText="Pinterest"></span><span class="st_reddit_large" displayText="Reddit"></span><span class="st_email_large" displayText="Email"></span></div>';  
   
      /* Add javascript for VShare */

      $Sender->Head->AddString("<script type=\"text/javascript\">var switchTo5x=true;</script><script type=\"text/javascript\" src=\"http://w.sharethis.com/button/buttons.js\"></script><script type=\"text/javascript\">stLight.options({publisher:'{$PublisherNumber},doNotHash: false, doNotCopy: false, hashAddressBar: false'});</script>");
      /* Display VShare buttons. */
      
      if (GetValue('Type', $Sender->EventArguments) == 'Discussion') {
         echo $VShareCode;
      }
   }     


   public function Setup() {
      // Nothing to do here!
   }
   
   public function Structure() {
      // Nothing to do here!
   }
   
  /*Add to dashboard side menu. */ 
  
   public function Base_GetAppSettingsMenuItems_Handler($Sender) {
      $Menu = $Sender->EventArguments['SideMenu'];
      $Menu->AddLink('Add-ons', T('VShare'), 'plugin/VShare', 'Garden.Settings.Manage');
   }

   public function PluginController_VShare_Create($Sender) {
      $Sender->Permission('Garden.Settings.Manage');
      $Sender->Title('VShare');
      $Sender->AddSideMenu('plugin/VShare');
      $Sender->Form = new Gdn_Form();
      $this->Dispatch($Sender, $Sender->RequestArgs);
   }
   
   /* Define variables for settings page. */
   
   public function Controller_Index($Sender) {
      $PublisherNumber = C('Vanilla.Plugin.PublisherNumber', 'Publisher Number');
      
      $Validation = new Gdn_Validation();
      $ConfigurationModel = new Gdn_ConfigurationModel($Validation);
      
      $ConfigArray = array(
         'Plugin.VShare.PublisherNumber'
      );
      if ($Sender->Form->AuthenticatedPostBack() === FALSE)
         $ConfigArray['Plugin.VShare.PublisherNumber'] = $PublisherNumber;
      
      $ConfigurationModel->SetField($ConfigArray);
      
      // Set the model on the form.
      $Sender->Form->SetModel($ConfigurationModel);
      
      // If seeing the form for the first time...
      if ($Sender->Form->AuthenticatedPostBack() === FALSE) {
         // Apply the config settings to the form.
         $Sender->Form->SetData($ConfigurationModel->Data);
      } else {
         // Define some validation rules for the fields being saved
         $ConfigurationModel->Validation->ApplyRule('Plugin.VShare.PublisherNumber', 'Required');
         
         if ($Sender->Form->Save() !== FALSE) {
            $Sender->StatusMessage = T("Your changes have been saved.");
         }
      }
      
      $Sender->Render($this->GetView('vshare.php'));
   }
      
}