<?php if (!defined('APPLICATION')) exit();

// Conversations
$Configuration['Conversations']['Version'] = '2.2.11';

// Database
$Configuration['Database']['Name'] = 'bbs';
$Configuration['Database']['Host'] = 'localhost';
$Configuration['Database']['User'] = 'root';
$Configuration['Database']['Password'] = '';

// EnabledApplications
$Configuration['EnabledApplications']['Conversations'] = 'conversations';
$Configuration['EnabledApplications']['Vanilla'] = 'vanilla';

// EnabledPlugins
$Configuration['EnabledPlugins']['HtmLawed'] = 'HtmLawed';
$Configuration['EnabledPlugins']['OpenID'] = TRUE;
$Configuration['EnabledPlugins']['GoogleSignIn'] = TRUE;
$Configuration['EnabledPlugins']['Facebook'] = TRUE;
$Configuration['EnabledPlugins']['Twitter'] = TRUE;
$Configuration['EnabledPlugins']['VanillaInThisDiscussion'] = TRUE;
$Configuration['EnabledPlugins']['VShare'] = TRUE;
$Configuration['EnabledPlugins']['Sprites'] = TRUE;
$Configuration['EnabledPlugins']['cleditor'] = TRUE;
$Configuration['EnabledPlugins']['AllViewed'] = TRUE;

// Garden
$Configuration['Garden']['Title'] = '';
$Configuration['Garden']['Cookie']['Salt'] = 'RDLDOKDMRO';
$Configuration['Garden']['Cookie']['Domain'] = '';
$Configuration['Garden']['Registration']['ConfirmEmail'] = '1';
$Configuration['Garden']['Registration']['SendConnectEmail'] = '1';
$Configuration['Garden']['Registration']['Method'] = 'Captcha';
$Configuration['Garden']['Registration']['ConfirmEmailRole'] = '3';
$Configuration['Garden']['Registration']['CaptchaPrivateKey'] = '6LcKmvISAAAAAB5FkkFXYwSiRWhjNcnzawJsGaVw';
$Configuration['Garden']['Registration']['CaptchaPublicKey'] = '6LcKmvISAAAAAJlduwbbmpxWzTCc00gB2iwCEIdh';
$Configuration['Garden']['Registration']['InviteExpiration'] = '1 week';
$Configuration['Garden']['Registration']['InviteRoles']['3'] = '0';
$Configuration['Garden']['Registration']['InviteRoles']['4'] = '0';
$Configuration['Garden']['Registration']['InviteRoles']['8'] = '0';
$Configuration['Garden']['Registration']['InviteRoles']['16'] = '0';
$Configuration['Garden']['Registration']['InviteRoles']['32'] = '0';
$Configuration['Garden']['Email']['SupportName'] = 'Baby Speed';
$Configuration['Garden']['InputFormatter'] = 'Html';
$Configuration['Garden']['Version'] = '2.2.11';
$Configuration['Garden']['RewriteUrls'] = TRUE;
$Configuration['Garden']['CanProcessImages'] = TRUE;
$Configuration['Garden']['SystemUserID'] = '2';
$Configuration['Garden']['Installed'] = TRUE;
$Configuration['Garden']['InstallationID'] = '9ADB-D85F152D-8D840D4E';
$Configuration['Garden']['InstallationSecret'] = '8764db722dd121b712322008f6c6893721efcd7a';
$Configuration['Garden']['Theme'] = 'bittersweet';
$Configuration['Garden']['Html']['SafeStyles'] = FALSE;
$Configuration['Garden']['Embed']['Allow'] = FALSE;
$Configuration['Garden']['HomepageTitle'] = 'Mom\'s Assistant';
$Configuration['Garden']['Description'] = '';
$Configuration['Garden']['Errors']['LogEnabled'] = TRUE;
$Configuration['Garden']['Errors']['LogFile'] = 'log/error_log';
$Configuration['Garden']['Errors']['MasterView'] = 'deverror.master.php';
$Configuration['Garden']['Logo'] = '7Z9ZR8RBQJ7P.jpg';
$Configuration['Garden']['MobileLogo'] = 'AOBQ493L28OP.jpg';
$Configuration['Garden']['ShareImage'] = 'WVTJ0P3BRWX7.jpg';
$Configuration['Garden']['Analytics']['AllowLocal'] = TRUE;

// Plugin
$Configuration['Plugin']['VShare']['PublisherNumber'] = '136541d2-a26b-4a3b-94d6-41201755260b';

// Plugins
$Configuration['Plugins']['GettingStarted']['Dashboard'] = '1';
$Configuration['Plugins']['GettingStarted']['Plugins'] = '1';
$Configuration['Plugins']['GettingStarted']['Categories'] = '1';
$Configuration['Plugins']['GettingStarted']['Discussion'] = '1';
$Configuration['Plugins']['GettingStarted']['Registration'] = '1';
$Configuration['Plugins']['GettingStarted']['Profile'] = '1';
$Configuration['Plugins']['SocialLogin']['Apikey'] = '13d76aaf-ed9b-45ef-ae5c-5312593aee8a';
$Configuration['Plugins']['SocialLogin']['Secretkey'] = 'd2e050f1-f522-465c-b93e-6b566bb6c87f';
$Configuration['Plugins']['SocialLogin']['Use_Api'] = 'CURL';
$Configuration['Plugins']['SocialLogin']['Sociallogintitle'] = '';
$Configuration['Plugins']['SocialLogin']['updateprofile'] = 'Yes';
$Configuration['Plugins']['SocialLogin']['Sociallogincolumns'] = '4';
$Configuration['Plugins']['SocialLogin']['Socialloginbackground'] = '';
$Configuration['Plugins']['SocialLogin']['Email_required'] = 'Yes';
$Configuration['Plugins']['SocialLogin']['Loginredirect'] = 'Loginredirect1';
$Configuration['Plugins']['SocialLogin']['Loginredirecturl'] = '';
$Configuration['Plugins']['SocialLogin']['Account_linking'] = 'Yes';
$Configuration['Plugins']['SocialLogin']['Emailtitle'] = 'Please enter your email address to proceed';
$Configuration['Plugins']['SocialLogin']['Emailerrortitle'] = 'Please enter your correct email address to proceed';
$Configuration['Plugins']['SocialLogin']['Mappingtitle'] = '';
$Configuration['Plugins']['SocialLogin']['SkipEmail'] = 'No';
$Configuration['Plugins']['SocialLogin']['lrwelcomeemail'] = 'Yes';
$Configuration['Plugins']['SocialLogin']['Enablesociallogin'] = 'Yes';
$Configuration['Plugins']['SocialLogin']['Enablesocialicon'] = 'small';
$Configuration['Plugins']['SocialShare']['Enablesocialsharing'] = 'Yes';
$Configuration['Plugins']['SocialShare']['Enablehorizontalsharing'] = 'Yes';
$Configuration['Plugins']['SocialShare']['Enableverticalsharing'] = '';
$Configuration['Plugins']['SocialShare']['Horizontalsharingtheme'] = 'single-image-theme-large';
$Configuration['Plugins']['SocialShare']['verticalsharingtheme'] = '';
$Configuration['Plugins']['SocialShare']['Verticalsharingtheme'] = '';
$Configuration['Plugins']['SocialShare']['Verticalsharingposition'] = 'topleft';
$Configuration['Plugins']['SocialShare']['Sharingoffset'] = '';
$Configuration['Plugins']['SocialShare']['loginRadiusLIrearrange'] = array('Facebook', 'Pinterest', 'GooglePlus', 'Twitter', 'LinkedIn');
$Configuration['Plugins']['SocialShare']['loginRadiuscounter'] = array('Facebook Like', 'Twitter Tweet', 'Pinterest Pin it', 'Google+ Share', 'Hybridshare');
$Configuration['Plugins']['SocialShare']['loginRadiusLIverticalrearrange'] = array('Facebook', 'Pinterest', 'GooglePlus', 'Twitter', 'LinkedIn');
$Configuration['Plugins']['SocialShare']['loginRadiusverticalcounter'] = array('Facebook Like', 'Twitter Tweet', 'Pinterest Pin it', 'Google+ Share', 'Hybridshare');
$Configuration['Plugins']['Facebook']['ApplicationID'] = '1446088942301012';
$Configuration['Plugins']['Facebook']['Secret'] = '2e592adc8f68ede69d84005a1eb38932';
$Configuration['Plugins']['Facebook']['UseFacebookNames'] = FALSE;
$Configuration['Plugins']['Facebook']['SocialSignIn'] = '1';
$Configuration['Plugins']['Facebook']['SocialReactions'] = FALSE;
$Configuration['Plugins']['Facebook']['SocialSharing'] = '1';
$Configuration['Plugins']['Twitter']['ConsumerKey'] = 'DO0JncdiLPBhRR82HdSAq0QgT';
$Configuration['Plugins']['Twitter']['Secret'] = '2TxfkXAKiI4FvQyR7GVmQeXqkI1oxTiZR2gaSEEeBiezr0YNNO';
$Configuration['Plugins']['Twitter']['SocialReactions'] = '1';
$Configuration['Plugins']['Twitter']['SocialSharing'] = '1';

// Routes
$Configuration['Routes']['DefaultController'] = array('categories', 'Internal');

// Vanilla
$Configuration['Vanilla']['Version'] = '2.2.11';
$Configuration['Vanilla']['Categories']['MaxDisplayDepth'] = '3';
$Configuration['Vanilla']['Categories']['DoHeadings'] = FALSE;
$Configuration['Vanilla']['Categories']['HideModule'] = FALSE;
$Configuration['Vanilla']['Categories']['Layout'] = 'table';
$Configuration['Vanilla']['Discussions']['Layout'] = 'table';
$Configuration['Vanilla']['Discussion']['SpamCount'] = '3';
$Configuration['Vanilla']['Discussion']['SpamTime'] = '60';
$Configuration['Vanilla']['Discussion']['SpamLock'] = '120';
$Configuration['Vanilla']['Comment']['SpamCount'] = '5';
$Configuration['Vanilla']['Comment']['SpamTime'] = '60';
$Configuration['Vanilla']['Comment']['SpamLock'] = '120';
$Configuration['Vanilla']['Comment']['MaxLength'] = '8000';
$Configuration['Vanilla']['Comment']['MinLength'] = '10';

// Last edited by admin (127.0.0.1)2014-06-05 16:58:31