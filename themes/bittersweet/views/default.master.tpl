<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://www.babyspeedy.com/commonstyle/style.css" type="text/css" rel="stylesheet" />
    <link href="http://www.babyspeedy.com/commonstyle/reset.css" type="text/css" rel="stylesheet" />
  {asset name="Head"}

</head>
<body id="{$BodyID}" class="{$BodyClass}">
<header>
    <div id="header">
        <div class="column1080">
            <div class="site-nav">
                <span class="subnav f_l">
                	<ul>
                        <li><a href="http://babyspeedy.com/">Home</a></li>
                        <li><a href="http://babyspeedy.com/shop.php">Shop</a></li>
                        <li><a href="http://like.babyspeedy.com/">DJJunMix</a></li>
                        <li><a href="http://bbs.babyspeedy.com/">Mom's Assistant</a></li>
                    </ul>
                </span>
                <span class="site-nav-r f_r">
                    <ul>
                        <li class="site-log">Welcome&nbsp;&nbsp;<a href="#">(Log In)</a></li>
                        <li class="site-Account"><a href="#">My Account</a></li>
                        <li class="site-Bag">Bag&nbsp;&nbsp;<a href="#">0</a></li>
                    </ul>
                </span>
            </div>
            <a href="#"><img src="http://www.babyspeedy.com/images/logov2.gif" class="indexlogo f_l" title="babaybuy" alt="babaybuy" /></a>
            <div class="nav-share f_r mt30">

                <a href="#" target="_blank"><img src="http://www.babyspeedy.com/images/subscriptionicon.png"/></a>
                <a href="https://www.facebook.com/ourbabyspeedy" target="_blank"><img src="http://www.babyspeedy.com/images/facebookicon.png"></a>
                <a href="http://www.pinterest.com/ourbabyspeedy"  target="_blank"><img src="http://www.babyspeedy.com/images/pinticon.png"/></a>
                <a href="https://twitter.com/ourbabyspeedy"  target="_blank"><img src="http://www.babyspeedy.com/images/twittericon.png"/></a>
            </div>
        </div>
    </div>
</header>
<div id="Frame">
  <div id="Head">
    <div class="Row">
      <div class="SiteSearch">{searchbox}</div>
      <ul class="SiteMenu">
          {home_link}
       {discussions_link}
       {activity_link}
       {inbox_link}
          {dashboard_link}
       {custom_menu}
       {profile_link}
       {signinout_link}
      </ul>
    </div>
  </div>
  <div class="BreadcrumbsWrapper">
    <div class="Row">
     {breadcrumbs}
    </div>
  </div>
  <div id="Body">
    <div class="Row">
        <div class="Column PanelColumn" id="Panel">
            {module name="MeModule"}
            {asset name="Panel"}
        </div>
      <div class="Column ContentColumn" id="Content">{asset name="Content"}</div>

    </div>
  </div>
  </div>

    {event name="AfterBody"}
</div>

<footer>
    <div id="footer" class="mt20">
        <div class="column1080">
            <ul class="aboutlist mt10">
                <li>
                    LET US HELP YOU
                    <a href="#">shipping &amp; Retrns</a>
                    <a href="#">Order Trachking</a>
                    <a href="#">My Account</a>
                    <a href="#">Gift Cards</a>
                </li>
                <li>
                    GET GONECTED
                    <a href="https://www.facebook.com/ourbabyspeedy"><img src="http://www.babyspeedy.com/images/facebookicon.png">Facebook</a>
                    <a href="https://twitter.com/ourbabyspeedy"><img src="http://www.babyspeedy.com/images/twittericon.png">Twitter</a>
                    <a href="http://www.pinterest.com/ourbabyspeedy"><img src="http://www.babyspeedy.com/images/pinticon.png">Pinterest</a>
                </li>
            </ul>
            <a href="#"><img src="http://www.babyspeedy.com/images/footerlogo.jpg" alt="baby buy" class="f_l"/></a>
            <div class="contactus mt10">
                <span>JOIN THE FUN</span>
                Subscribe to stay in the konw!
                <form class="mt20" action="emailsub" method="get" class="mt20" target="_blank" name="form" id="form" target="_self" method='get' >
                   	<input id="emailOrigin" name="emailOrigin" type="hidden" value="like.com" /> 
                    <input id="emailAddress" name="emailAddress" type="text"  placeholder="Enter Email" class="inputstyle01"/>
                    <input type="button" value="" onclick="document.form.submit();" class="input-but"/>
                 </form>
                <!--
                <form class="mt20">
                    <input type="text"  placeholder="Enter Email" class="inputstyle01"/><input type="button" value="" class="input-but"/>
                </form>
				-->
            </div>
        </div>
        <div class="footercopy mt20">
            Copyright&nbsp;&copy; 2013-2014 www.babyspeedy.com All Rights Reserved.
        </div>
    </div>
</footer>
</body>
</html>
