<!DOCTYPE html>
<html lang="en" class="sticky-footer-html">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {asset name="Head"}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  <link href="http://www.babyspeedy.com/commonstyle/style.css" type="text/css" rel="stylesheet" />
  <link href="http://www.babyspeedy.com/commonstyle/reset.css" type="text/css" rel="stylesheet" /> 
  </head>
  <body id="{$BodyID}" class="{$BodyClass} sticky-footer-body">

<div id="header">
        <div class="column1080">
            <div class="site-nav">
                <span class="subnav f_l">
                	<ul>
                    	<li><a href="#">Home</a></li>
                        <li><a href="#">Shop</a></li>
                        <li><a href="#">Like</a></li>
                        <li><a href="#">Mom's Assistant</a></li>
                    </ul>
                </span>
                <span class="site-nav-r f_r">
                    <ul>
                        <li class="site-log">Welcome&nbsp;&nbsp;<a href="#">(Log In)</a></li> 
                        <li class="site-Account"><a href="#">My Account</a></li>
                        <li class="site-Bag">Bag&nbsp;&nbsp;<a href="#">0</a></li> 
                    </ul> 
                </span>
            </div>  
            <a href="#"><img src="/images/logov2.gif" class="indexlogo f_l" title="babaybuy" alt="babaybuy" /></a>    
             <div class="nav-share f_r mt30">
                <form>
                    <input type="text" class="inputstyle01" tabindex="1" placeholder="Start typing here..."/><input type="button" value="" class="input-but"/>
                </form>
                <a href="#"><img src="/images/subscriptionicon.png"/></a>
                <a href="#"><img src="/images/facebookicon.png"></a>
                <a href="#"><img src="/images/pinticon.png"/></a>
                <a href="#"><img src="/images/twittericon.png"/></a>
             </div>
         </div>
     </div>  


    <nav class="navbar navbar-default navbar-static-top" role="navigation" >
      <div class="container">

        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">{t c="Toggle navigation"}</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{link path="home"}">{logo}</a>
        </div>

        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            {home_link}
            {categories_link}
            {discussions_link}
            {activity_link}
          </ul>
          <ul class="nav navbar-nav navbar-right">
            {if $User.SignedIn}
              {module name="MeModule" CssClass="hidden-xs"}
            {else}
              <li>{link path="register" text="Register" target="current"}</li>
              <li>{link path="signin" text="Sign In" target="current"}</li>
            {/if}
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <section class="container">
      <div class="row">

        <main class="page-content" role="main">
          {breadcrumbs}
            {if InSection(array("CategoryList", "CategoryDiscussionList", "DiscussionList"))}
                <div class="SearchForm">{searchbox}</div>
            {/if}
            {asset name="Content"}
        </main>

        <aside class="page-sidebar" role="complementary">
            {asset name="Panel"}
        </aside>

      </div>
    </section>


<footer>
    <div id="footer" class="mt20">
        <div class="column1080">
             <ul class="aboutlist mt10">
                <li> 
                    LET US HELP YOU
                    <a href="#">shipping &amp; Retrns</a>
                    <a href="#">Order Trachking</a>
                    <a href="#">My Account</a>
                    <a href="#">Gift Cards</a>
                </li>
                <li> 
                    GET GONECTED
                    <a href="#"><img src="/images/facebookicon.png">Facebook</a>
                    <a href="#"><img src="/images/twittericon.png">Twitter</a>
                    <a href="#"><img src="/images/pinticon.png">Pinterest</a>
                </li> 
             </ul>
             <a href="#"><img src="/images/footerlogo.jpg" alt="baby buy" class="f_l"/></a>
             <div class="contactus mt10">
                <span>JOIN THE FUN</span>
                 Subscribe to stay in the konw!
                 <form class="mt20">
                    <input type="text"  placeholder="Enter Email" class="inputstyle01"/><input type="button" value="" class="input-but"/>
                 </form>
             </div>
        </div>
        <div class="footercopy mt20">
            Copyright&nbsp;&copy; 2013-2014 www.buybuybaby.com All Rights Reserved.
        </div>
    </div>
</footer>


    {event name="AfterBody"}

  </body>
</html>
